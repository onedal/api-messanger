require 'rails_helper'

module Api
  module V1


    RSpec.describe AppsController, :type => :request do
      before do
        FactoryGirl.create :app,  title: 'app1', email: Faker::Internet.email
        FactoryGirl.create :app,  title: 'app2', email: Faker::Internet.email
      end

      describe 'GET /apps' do
        it 'returns all the apps' do
          get '/api/v1/apps'
          expect(response.status).to eq 200

          app_names =  JSON.parse(response.body)
                               .map{|app| app['title'] }
          expect(app_names).to include('app1')
          expect(app_names).to include('app2')
        end
      end


      describe 'GET /apps/:id 'do
        it 'returns the specified app' do
          id = App.find_by(title: 'app1').id
          get "/api/v1/apps/#{id}"

          expect(response.status).to eq 200

          body = JSON.parse(response.body)
          expect(body['title']) == 'app1'
          expect(body['id']) == id
        end
      end

      describe 'POST /apps' do
        it 'creates the specified app' do
          app = {
                  'title' => 'new_app',
                  'email' => 'security@gmail.com',
                  'default_uri' => 'http://billy.com'
          }

          post '/api/v1/apps', params: app

          expect(response.status).to eq 201
          body = JSON.parse(response.body)

          app_title = body['title']
          expect(app_title) == 'new_app'
        end
      end

      describe 'PUT /apps/1' do
        it 'update the specified app' do
          id = App.find_by(title: 'app1').id
          app = { 'email' => 'sunrise@gmail.com' }

          put "/api/v1/apps/#{id}", params: app

          body = JSON.parse(response.body)

          expect(response.status).to eq(200)
          expect(body['email']).to eq('sunrise@gmail.com')

        end
      end

      describe 'DELETE /apps/:id' do
        it 'deletes the specified app' do
          id = App.find_by(title: 'app1').id
          delete "/api/v1/apps/#{id}"
          expect(response.status).to eq 204
        end
      end

    end

  end
end