require 'rails_helper'

RSpec.describe App, :type => :model do

	describe 'associations' do
		it { should have_many(:users) }
	end

	describe 'db structure' do
	    it { is_expected.to have_db_column(:title).of_type(:string) }
	    it { is_expected.to have_db_column(:email).of_type(:string) }
	    it { is_expected.to have_db_column(:key).of_type(:text) }
	    it { is_expected.to have_db_column(:secret).of_type(:text) }
	    it { is_expected.to have_db_column(:default_uri).of_type(:text) }
	    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
	    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    end

    describe 'validations' do
	    it { should validate_presence_of(:title) }
	    it { should validate_presence_of(:email) }

	    describe 'uniqueness' do
		    subject { create(:app) }
	        it { should validate_uniqueness_of(:email)}
	    end
    end
end
