require 'rails_helper'

RSpec.describe ChatsUser, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:chat) }
  end

  describe 'db structure' do
    it { is_expected.to have_db_column(:user_id).of_type(:integer) }
    it { is_expected.to have_db_column(:chat_id).of_type(:integer) }
  end
end
