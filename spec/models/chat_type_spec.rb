require 'rails_helper'

RSpec.describe ChatType, type: :model do
  describe 'associations' do
    it { should have_many(:chats) }
  end

  describe 'db structure' do
    it { is_expected.to have_db_column(:name).of_type(:string) }
  end
end
