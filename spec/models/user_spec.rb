require 'rails_helper'

require 'rails_helper'

RSpec.describe User, :type => :model do

	describe 'associations' do
		it { should have_and_belong_to_many(:chats) }
		it { should belong_to(:app) }
	end

	describe 'db structure' do
		it { is_expected.to have_db_column(:app_id).of_type(:integer) }
		it { is_expected.to have_db_column(:email).of_type(:string) }
		it { is_expected.to have_db_column(:key).of_type(:text) }
		it { is_expected.to have_db_column(:secret).of_type(:text) }
		it { is_expected.to have_db_column(:user_name).of_type(:string) }
		it { is_expected.to have_db_column(:first_name).of_type(:string) }
		it { is_expected.to have_db_column(:last_name).of_type(:string) }
		it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
		it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
	end

	describe 'validations' do
		it { should validate_presence_of(:user_name) }
		it { should validate_presence_of(:email) }

		describe 'uniqueness' do
			subject { create(:user, app_id: create(:app).id) }
			it { should validate_uniqueness_of(:email)}
		end
	end
end