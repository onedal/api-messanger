require 'rails_helper'

RSpec.describe Chat, :type => :model do

  describe 'associations' do
    it { should have_and_belong_to_many(:users) }
    it { should belong_to(:chat_type) }
  end

  describe 'db structure' do
    it { is_expected.to have_db_column(:title).of_type(:string) }
  end

end
