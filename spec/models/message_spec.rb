require 'rails_helper'

RSpec.describe Message, type: :model do
  describe 'associations' do
    it { should belong_to(:chat) }
  end

  describe 'db structure' do
    it { is_expected.to have_db_column(:message_text).of_type(:string) }
    it { is_expected.to have_db_column(:chat_id).of_type(:integer) }
    it { is_expected.to have_db_column(:autor_uid).of_type(:integer) }
  end
end
