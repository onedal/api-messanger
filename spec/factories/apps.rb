FactoryGirl.define do
	factory :app do
		title Faker::Name.title
		email Faker::Internet.email
	end
	factory :app2, class: App do
		title Faker::Name.title
		email Faker::Internet.email
	end
end
