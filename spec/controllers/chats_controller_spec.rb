require 'rails_helper'


module Api
  module V1
    RSpec.describe ChatsController, :type => :request do

      let(:valid_attributes) {
        skip(title: Faker::Name.title, type_id: 1)
      }

      let(:invalid_attributes) {
        skip(title: Faker::Name.title, email: 1)
      }


      let!(:chat) { Chat.create(valid_attributes) }

      describe "GET #index" do
        it "assigns all chats as @chats" do
          #chat = Chat.create! valid_attributes
          get '/api/v1/chats'
          p response.body
          expect(response.status).to eq 200
          #expect(assigns(:chats)).to eq([chat])
        end
      end
      #
      # describe "GET #show" do
      #   it "assigns the requested chat as @chat" do
      #     chat = Chat.create! valid_attributes
      #     get :show, {:id => chat.to_param}, valid_session
      #     expect(assigns(:chat)).to eq(chat)
      #   end
      # end
      #
      # describe "GET #new" do
      #   it "assigns a new chat as @chat" do
      #     get :new, {}, valid_session
      #     expect(assigns(:chat)).to be_a_new(Chat)
      #   end
      # end
      #
      # describe "GET #edit" do
      #   it "assigns the requested chat as @chat" do
      #     chat = Chat.create! valid_attributes
      #     get :edit, {:id => chat.to_param}, valid_session
      #     expect(assigns(:chat)).to eq(chat)
      #   end
      # end
      #
      # describe "POST #create" do
      #   context "with valid params" do
      #     it "creates a new Chat" do
      #       expect {
      #         post :create, {:chat => valid_attributes}, valid_session
      #       }.to change(Chat, :count).by(1)
      #     end
      #
      #     it "assigns a newly created chat as @chat" do
      #       post :create, {:chat => valid_attributes}, valid_session
      #       expect(assigns(:chat)).to be_a(Chat)
      #       expect(assigns(:chat)).to be_persisted
      #     end
      #
      #     it "redirects to the created chat" do
      #       post :create, {:chat => valid_attributes}, valid_session
      #       expect(response).to redirect_to(Chat.last)
      #     end
      #   end
      #
      #   context "with invalid params" do
      #     it "assigns a newly created but unsaved chat as @chat" do
      #       post :create, {:chat => invalid_attributes}, valid_session
      #       expect(assigns(:chat)).to be_a_new(Chat)
      #     end
      #
      #     it "re-renders the 'new' template" do
      #       post :create, {:chat => invalid_attributes}, valid_session
      #       expect(response).to render_template("new")
      #     end
      #   end
      # end
      #
      # describe "PUT #update" do
      #   context "with valid params" do
      #     let(:new_attributes) {
      #       skip("Add a hash of attributes valid for your model")
      #     }
      #
      #     it "updates the requested chat" do
      #       chat = Chat.create! valid_attributes
      #       put :update, {:id => chat.to_param, :chat => new_attributes}, valid_session
      #       chat.reload
      #       skip("Add assertions for updated state")
      #     end
      #
      #     it "assigns the requested chat as @chat" do
      #       chat = Chat.create! valid_attributes
      #       put :update, {:id => chat.to_param, :chat => valid_attributes}, valid_session
      #       expect(assigns(:chat)).to eq(chat)
      #     end
      #
      #     it "redirects to the chat" do
      #       chat = Chat.create! valid_attributes
      #       put :update, {:id => chat.to_param, :chat => valid_attributes}, valid_session
      #       expect(response).to redirect_to(chat)
      #     end
      #   end
      #
      #   context "with invalid params" do
      #     it "assigns the chat as @chat" do
      #       chat = Chat.create! valid_attributes
      #       put :update, {:id => chat.to_param, :chat => invalid_attributes}, valid_session
      #       expect(assigns(:chat)).to eq(chat)
      #     end
      #
      #     it "re-renders the 'edit' template" do
      #       chat = Chat.create! valid_attributes
      #       put :update, {:id => chat.to_param, :chat => invalid_attributes}, valid_session
      #       expect(response).to render_template("edit")
      #     end
      #   end
      # end
      #
      # describe "DELETE #destroy" do
      #   it "destroys the requested chat" do
      #     chat = Chat.create! valid_attributes
      #     expect {
      #       delete :destroy, {:id => chat.to_param}, valid_session
      #     }.to change(Chat, :count).by(-1)
      #   end
      #
      #   it "redirects to the chats list" do
      #     chat = Chat.create! valid_attributes
      #     delete :destroy, {:id => chat.to_param}, valid_session
      #     expect(response).to redirect_to(chats_url)
      #   end
      # end

    end
  end
end

