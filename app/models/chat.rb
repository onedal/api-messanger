class Chat < ApplicationRecord
  belongs_to :chat_type, foreign_key: :type_id
  has_and_belongs_to_many :users
end


# chat.users << user
# TODO Chat for users only one app