class ChatsUser < ApplicationRecord
  belongs_to :user, foreign_key: :user_id
  belongs_to :chat, foreign_key: :chat_id
end
