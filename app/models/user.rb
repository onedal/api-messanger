class User < ApplicationRecord
	belongs_to :app
	has_and_belongs_to_many :chats

	validates :email, :user_name, presence: true
	validates :email, uniqueness: true


	before_save :downcase_fields

	def downcase_fields
		self.email.downcase!
	end

end
