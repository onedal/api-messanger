class App < ApplicationRecord

	has_many :users

	validates :email, :title, presence: true
	validates :email, uniqueness: true


	before_save :downcase_fields

	def downcase_fields
		self.email.downcase!
	end


end
