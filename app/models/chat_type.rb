class ChatType < ApplicationRecord
	has_many :chats, foreign_key: :type_id
end
