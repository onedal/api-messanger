module Api
  module V1


    class ChatsController < ApplicationController
      before_action :set_chat, only: [:show, :update, :destroy]
      #respond_with :api, :v1, @chat
      # GET /chats
      def index
        @chats = Chat.all

        render json: @chats
      end

      # GET /chats/1
      def show
        render json: @chat
      end

      # POST /chats
      def create
        @chat = Chat.new(chat_params)

        if @chat.save
          render json: @chat, status: :created, location: api_v1_chats_url
        else
          render json: @chat.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /chats/1
      def update
        if @chat.update(chat_params)
          render json: @chat
        else
          render json: @chat.errors, status: :unprocessable_entity
        end
      end

      # DELETE /chats/1
      def destroy
        @chat.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_chat
          @chat = Chat.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def chat_params
          params.require(:chat).permit(:type_id, :title)
        end
    end
  end
end