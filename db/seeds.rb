# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

100.times do
	App.create(
			title: Faker::Name.title,
			email: Faker::Internet.email,
			key: Faker::Bitcoin.address,
			secret: Faker::Bitcoin.address,
			default_uri: Faker::Internet.url
	)
end

App.all.each do |app|
	count = rand(5..15)
	count.times { app.users.create(
			user_name: Faker::Internet.user_name,
			first_name: Faker::Name.first_name,
			last_name: Faker::Name.last_name,
			email: Faker::Internet.email,
			key: Faker::Bitcoin.address,
			secret: Faker::Bitcoin.address) }
end

