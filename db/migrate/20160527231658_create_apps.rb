class CreateApps < ActiveRecord::Migration[5.0]
  def change
    create_table :apps do |t|
      t.string :title, null: false
      t.string :email, unique: true
      t.text :key
      t.text :secret
      t.text :default_uri
      t.timestamps
    end
  end
end
