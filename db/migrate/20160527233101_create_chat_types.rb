class CreateChatTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :chat_types do |t|
      t.string :name
    end
  end
end
