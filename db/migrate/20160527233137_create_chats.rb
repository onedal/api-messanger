class CreateChats < ActiveRecord::Migration[5.0]
  def change
    create_table :chats do |t|
      t.integer :type_id, foreign_key: true , null: false
      t.string :title , null: false

      t.timestamps
    end
  end
end
