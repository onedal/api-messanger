class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.integer :app_id , null: false
      t.string :user_name, null: false
      t.string :first_name
      t.string :last_name
      t.string :email, unique: true
      t.text :key
      t.text :secret
      t.timestamps
    end
  end
end
