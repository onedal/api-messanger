class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.integer :chat_id, foreign_key: true, null: false
      t.integer :autor_uid , null: false
      t.string :message_text

      t.timestamps
    end
  end
end
